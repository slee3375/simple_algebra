package sps.sarah.simple_calculate;

public class Calculate {

    /**
     * Add two numbers
     * @param a int number
     * @param b int number
     * @return result of a add b
     */
    public static int addNumbers(int a, int b){
        return a+b;
    }

    /**
     * Subtract two numbers
     * @param a int number
     * @param b int number
     * @return result of a sumtract b
     */
    public static int subtractNumbers(int a, int b){
        return a-b;
    }
}
