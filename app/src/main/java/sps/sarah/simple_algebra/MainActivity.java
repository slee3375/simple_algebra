package sps.sarah.simple_algebra;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import sps.sarah.simple_calculate.Calculate;

public class MainActivity extends AppCompatActivity {

    Button button;
    TextView result;
    EditText firstNumber;
    EditText secondNumber;

    int first = 0;
    int second = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        setView();
    }

    private void setView(){
        button = findViewById(R.id.button);
        result = findViewById(R.id.result);
        firstNumber = findViewById(R.id.first_number);
        secondNumber = findViewById(R.id.second_number);

        button.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculate();
            }
        });
    }

    private void calculate(){
        int firstInt = 0;
        int secondInt = 0;
        String resultInt = "None";

        try {
            firstInt = Integer.parseInt(firstNumber.getText().toString());
            secondInt = Integer.parseInt(secondNumber.getText().toString());

            //TODO put calculate here
            resultInt = Calculate.addNumbers(firstInt,secondInt) + "";

        }catch (Exception e){
            resultInt = "error";
        }

        result.setText(resultInt);
    }

}